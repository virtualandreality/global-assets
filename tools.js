/*
    Useful JS tools
 */
let tools = {
    rand: function(a,b){
        return Math.floor(Math.random() * b) + a;
    },
    randKey: function(arr){
        let max = arr.length -1 ;
        return Math.floor(Math.random() * max) + 0;
    },
    time: function(){

        let date = new Date();

        let hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;

        let min  = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;

        let sec  = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;

        let year = date.getFullYear();

        let month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;

        let day  = date.getDate();
        day = (day < 10 ? "0" : "") + day;

        return year + "-" + month + "-" + day + "-" + hour + "-" + min + "-" + sec;
    },
    dateTime: function(){
        dt = new Date().toLocaleString();
        return dt
    },
    everyNth: function(arr,nth){
        let x = arr.filter((element, index) => {
            return index % nth === 0;
        });
        return x;
    },
    inArray: function(needle,haystack){
        let length = haystack.length;
        for(let i = 0; i < length; i++){
            if(haystack[i] === needle) return true;
        }
        return false;
    },
    onReady: function(f){
        // use like: tools.onReady(function(){...});
        /in/.test(document.readyState)?setTimeout('tools.onReady('+f+')',9):f()
    }
};

$ = function(selector){
    let symbol = selector.charAt(0);
    selector = selector.substr(1);
    if(symbol === '#'){
        return document.getElementById(selector)[0]
    }else if(symbol === '.'){
        return document.getElementsByClassName(selector)[0]
    }
}