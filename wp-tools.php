<?php
/*
 * Wordpress
 */

function is_dev(){
	if(current_user_can('administrator'))
		return true;
	return false;
}


if(function_exists('add_theme_support')){
	add_theme_support('menus');
	add_theme_support( 'custom-logo' );
}