<?php


/*
 * HTML Shortcodes
 */

function bgi($val){
	echo 'background-image:url('.$val.');';
}

function bg($val){
	echo 'background:url('.$val.');';
}



/*
 * Debugging
 */

function vardump($dump){
	echo '<pre style="'.bg('pink').'">';
	vardump($dump);
	echo '<pre>';
}